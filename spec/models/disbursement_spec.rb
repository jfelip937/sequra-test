require 'rails_helper'

RSpec.describe Disbursement, type: :model do
  let!(:merchant) { FactoryBot.create :merchant }
  let!(:shopper) { FactoryBot.create :shopper }
  let!(:order_completed1) { FactoryBot.create :order, merchant_id: merchant.id, shopper_id: shopper.id, completed_at: DateTime.current, amount: 30 }
  let!(:order_completed2) { FactoryBot.create :order, merchant_id: merchant.id, shopper_id: shopper.id, completed_at: DateTime.current, amount: 60 }
  let!(:order_completed3) { FactoryBot.create :order, merchant_id: merchant.id, shopper_id: shopper.id, completed_at: DateTime.current, amount: 310 }
  let!(:order_pending) { FactoryBot.create :order, merchant_id: merchant.id, shopper_id: shopper.id }

  describe "calculate" do

    it "only creates from completed orders" do
      Disbursement.calculate
      expect(Disbursement.count).to eq 3
    end

    it "only creates from not disbursed orders" do
      Disbursement.create(order_id: order_completed1.id, merchant_id: merchant.id, amount: 1, order_completed_at: DateTime.current)
      count = Disbursement.count
      Disbursement.calculate
      expect(Disbursement.count).to eq(count + 2)
    end

    context "percentages" do

      before do
        Disbursement.calculate
      end

      it 'create disbursement of 1% if order amount is less than 50' do
        expect(Disbursement.find_by(order_id: order_completed1.id).amount).to eq 0.3
      end

      it 'create disbursement of 0.5% if order amount is between 50 and 300' do
        expect(Disbursement.find_by(order_id: order_completed2.id).amount).to eq 0.57
      end

      it 'create disbursement of 0.85% if order amount is between 50 and 300' do
        expect(Disbursement.find_by(order_id: order_completed3.id).amount).to eq 2.64
      end
    end
  end
end