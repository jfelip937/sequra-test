Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :disbursements, except: [:create, :show, :new, :edit, :update, :destroy, :index] do
    collection do
      get :by_week
    end
  end
end
