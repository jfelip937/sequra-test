# SEQURA TESET

This project is for testing developing skills.

Is based on a system of merchants, shoppers and orders. Merchants get a disbursment for every completed order, the amount of this disbursment will depend on the amount of the order.

**SetUp**

* bundle install

Before setting up the database edit the /config/database.yml with your local configuration. You can also use server environment variables or add them to a .env file in the project root.

* rails db:create
* rails db:migrate
* rails db:seed

**Usage**

To create disbursements from completed orders, not already disbursed. In rails console:
 * Disbursement.calculate

API examples:

* /disbursements/by_week?date=01/04/2018&merchant_id=1
* /disbursements/by_week?date=01/04/2018

**TODO**

* Create a Active Job that call Disbursement.calculate.
* Schedule this job for every monday. Use whenever gem or redis.
* Add more tests.

**Technical Choises**
* I remove all routes created by resources, but in a normal project we will use some of this routes. This is why I choosed this option before namespace or hardcoding the route.
* I understand a week as seven days from the given date.
* Disbursement.calculate could be placed in a modul under /app/services folder.