require 'rails_helper'

RSpec.describe "DisbursementsController", type: :request do
  let!(:merchant) { FactoryBot.create :merchant }
  let!(:shopper) { FactoryBot.create :shopper }
  let!(:order_completed1) { FactoryBot.create :order, merchant_id: merchant.id, shopper_id: shopper.id, completed_at: DateTime.current, amount: 30 }
  let!(:order_completed2) { FactoryBot.create :order, merchant_id: merchant.id, shopper_id: shopper.id, completed_at: DateTime.current, amount: 60 }
  let!(:order_completed3) { FactoryBot.create :order, merchant_id: merchant.id, shopper_id: shopper.id, completed_at: DateTime.current, amount: 310 }
  let!(:order_pending) { FactoryBot.create :order, merchant_id: merchant.id, shopper_id: shopper.id }

  let(:params) do
    {
      "date": Date.current - 2.days
    }
  end

  describe "GET /by_week" do
    before do
      Disbursement.calculate
    end

    context "with incorrect params" do
      it "returns bad request" do
        get by_week_disbursements_url, params: {}
        expect(response.status).to eq 400
      end
    end

    context "with correct params" do
      it "returns 200" do
        get by_week_disbursements_url, params: params
        expect(response.status).to eq 200
      end

      it "returns an Array" do
        get by_week_disbursements_url, params: params
        body_hash = JSON.parse(response.body)
        expect(body_hash.class.name).to eq "Array"
      end
    end
  end
end