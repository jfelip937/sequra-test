class Order < ApplicationRecord
  belongs_to :merchant
  belongs_to :shopper
  has_one :disbursement

  scope :completed, -> { where.not(completed_at: nil) }
  scope :pending, -> { where(completed_at: nil) }
  scope :disbursed, -> { where(id: Disbursement.pluck(:order_id).uniq) }
  scope :not_disbursed, -> { where.not(id: Disbursement.pluck(:order_id).uniq) }

end
