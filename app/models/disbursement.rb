class Disbursement < ApplicationRecord
  belongs_to :order
  belongs_to :merchant

  def self.calculate
    Order.completed.not_disbursed.each do |order|
      percentage= case order.amount
              when 0..49.99
                1
              when 50..299.99
                0.95
              when 300..Float::INFINITY
                0.85
              end

      amount = percentage*order.amount/100
      Disbursement.create!(
        order: order,
        merchant: order.merchant,
        amount: amount.round(2),
        order_completed_at: order.completed_at
      )
    end
  end
end
