class DisbursementsController < ApplicationController

  def by_week
    if params[:date]
      date = params[:date].to_date
      end_date = date + 7.days
      disbursements = Disbursement.where(order_completed_at: date..end_date)
      if params[:merchant_id]
        disbursements = disbursements.where(merchant_id: params[:merchant_id])
      end
      render json: disbursements
    else
      head :bad_request
    end
  end
end
