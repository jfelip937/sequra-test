merchants = JSON.parse(File.read(Rails.root.join('storage', 'merchants.json')))
merchants.each do |merchant|
  Merchant.create!(merchant.to_h)
end
shoppers = JSON.parse(File.read(Rails.root.join('storage', 'shoppers.json')))
shoppers.each do |shopper|
  Shopper.create!(shopper.to_h)
end
orders = JSON.parse(File.read(Rails.root.join('storage', 'orders.json')))
orders.each do |order|
  Order.create!(order.to_h)
end