class AddOrderCompletedAtToDisbursements < ActiveRecord::Migration[6.0]
  def change
    add_column :disbursements, :order_completed_at, :timestamp
  end
end
