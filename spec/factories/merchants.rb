FactoryBot.define do
  factory :merchant do
    sequence(:name) { |n| "max#{n} power" }
    sequence(:email) { |n| "max#{n}@power.com1234" }
  end
end